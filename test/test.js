var assert = require('assert');
describe('HelloWorld Module', function() {
  
    it('should return -1 when "Hello" is missing', function() {
    assert.equal(-1, "Hallo World".indexOf("Hello"));
  });


  it('should return 0 when sentence starts with Hello', function() {
    assert.equal(0, "Hello World, how are you?".indexOf("Hello"));
  });


});




//var request = require('supertest');
//var app = require('../server.js')
//describe('GET /', function() {
 //it('displays "Hello World!"', function(done) {
// // The line below is the core test of our app.
//request(app).get('/').expect('Hello World!', done);
// });
//});